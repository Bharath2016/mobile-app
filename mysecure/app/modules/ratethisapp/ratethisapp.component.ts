import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Ratings } from "nativescript-ratings";
@Component({
  selector: 'ratethisapp',
  templateUrl: 'modules/ratethisapp/ratethisapp.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RatethisappComponent {
  constructor() {
    let ratings = new Ratings(
      {
          id: "0",
          showOnCount: 2,
          title: "What do you think?",
          text: "If you like this app, please take a moment to leave a positive rating.",
          agreeButtonText: "Rate Now",
          remindButtonText: "Remind Me Later",
          declineButtonText: "No Thanks",
          androidPackageId: "com.nativescript.demo",
          iTunesAppId: "927183647"
      }
  );
    ratings.init();
    ratings.prompt();
}
}
