"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AccountsettingsComponent = /** @class */ (function () {
    function AccountsettingsComponent() {
        this.text = 'Account Settings';
    }
    AccountsettingsComponent = __decorate([
        core_1.Component({
            selector: 'accountsettings',
            templateUrl: 'modules/accountsettings/accountsettings.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        })
    ], AccountsettingsComponent);
    return AccountsettingsComponent;
}());
exports.AccountsettingsComponent = AccountsettingsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudHNldHRpbmdzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjY291bnRzZXR0aW5ncy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBbUU7QUFPbkU7SUFMQTtRQU1FLFNBQUksR0FBVyxrQkFBa0IsQ0FBQztJQUNwQyxDQUFDO0lBRlksd0JBQXdCO1FBTHBDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLFdBQVcsRUFBRSx3REFBd0Q7WUFDckUsZUFBZSxFQUFFLDhCQUF1QixDQUFDLE1BQU07U0FDaEQsQ0FBQztPQUNXLHdCQUF3QixDQUVwQztJQUFELCtCQUFDO0NBQUEsQUFGRCxJQUVDO0FBRlksNERBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhY2NvdW50c2V0dGluZ3MnLFxuICB0ZW1wbGF0ZVVybDogJ21vZHVsZXMvYWNjb3VudHNldHRpbmdzL2FjY291bnRzZXR0aW5ncy5jb21wb25lbnQuaHRtbCcsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIEFjY291bnRzZXR0aW5nc0NvbXBvbmVudCB7XG4gIHRleHQ6IHN0cmluZyA9ICdBY2NvdW50IFNldHRpbmdzJztcbn1cbiJdfQ==