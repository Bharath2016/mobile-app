"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var UsersComponent = /** @class */ (function () {
    function UsersComponent() {
        this.text = 'Users';
    }
    UsersComponent = __decorate([
        core_1.Component({
            selector: 'users',
            templateUrl: 'modules/users/users.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        })
    ], UsersComponent);
    return UsersComponent;
}());
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXNlcnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQW1FO0FBT25FO0lBTEE7UUFNRSxTQUFJLEdBQVcsT0FBTyxDQUFDO0lBQ3pCLENBQUM7SUFGWSxjQUFjO1FBTDFCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsT0FBTztZQUNqQixXQUFXLEVBQUUsb0NBQW9DO1lBQ2pELGVBQWUsRUFBRSw4QkFBdUIsQ0FBQyxNQUFNO1NBQ2hELENBQUM7T0FDVyxjQUFjLENBRTFCO0lBQUQscUJBQUM7Q0FBQSxBQUZELElBRUM7QUFGWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndXNlcnMnLFxuICB0ZW1wbGF0ZVVybDogJ21vZHVsZXMvdXNlcnMvdXNlcnMuY29tcG9uZW50Lmh0bWwnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBVc2Vyc0NvbXBvbmVudCB7XG4gIHRleHQ6IHN0cmluZyA9ICdVc2Vycyc7XG59XG4iXX0=