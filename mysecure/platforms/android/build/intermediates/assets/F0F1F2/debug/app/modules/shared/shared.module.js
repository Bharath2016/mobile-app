"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var side_drawer_directives_1 = require("nativescript-telerik-ui/sidedrawer/angular/side-drawer-directives");
var side_drawer_page_1 = require("./side-drawer-page");
var borderless_btn_directive_1 = require("./borderless-btn.directive");
var app_service_1 = require("./app.service");
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        core_1.NgModule({
            providers: [
                app_service_1.ActivityService
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                side_drawer_directives_1.NativeScriptUISideDrawerModule,
            ],
            declarations: [
                side_drawer_page_1.SideDrawerPageComponent,
                borderless_btn_directive_1.BorderlessBtnDirective
            ],
            exports: [
                side_drawer_page_1.SideDrawerPageComponent,
                borderless_btn_directive_1.BorderlessBtnDirective
            ]
        })
    ], SharedModule);
    return SharedModule;
}());
exports.SharedModule = SharedModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNoYXJlZC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUM7QUFFekMsZ0ZBQThFO0FBQzlFLDRHQUFtSDtBQUVuSCx1REFBNkQ7QUFDN0QsdUVBQW9FO0FBQ3BFLDZDQUFnRDtBQW1CaEQ7SUFBQTtJQUVBLENBQUM7SUFGWSxZQUFZO1FBakJ4QixlQUFRLENBQUM7WUFDUixTQUFTLEVBQUU7Z0JBQ1QsNkJBQWU7YUFDbEI7WUFDQyxPQUFPLEVBQUU7Z0JBQ1Asd0NBQWtCO2dCQUNsQix1REFBOEI7YUFDL0I7WUFDRCxZQUFZLEVBQUU7Z0JBQ1osMENBQXVCO2dCQUN2QixpREFBc0I7YUFDdkI7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsMENBQXVCO2dCQUN2QixpREFBc0I7YUFDdkI7U0FDRixDQUFDO09BQ1csWUFBWSxDQUV4QjtJQUFELG1CQUFDO0NBQUEsQUFGRCxJQUVDO0FBRlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlJztcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFVJU2lkZURyYXdlck1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC10ZWxlcmlrLXVpL3NpZGVkcmF3ZXIvYW5ndWxhci9zaWRlLWRyYXdlci1kaXJlY3RpdmVzJztcblxuaW1wb3J0IHsgU2lkZURyYXdlclBhZ2VDb21wb25lbnQgfSBmcm9tICcuL3NpZGUtZHJhd2VyLXBhZ2UnO1xuaW1wb3J0IHsgQm9yZGVybGVzc0J0bkRpcmVjdGl2ZSB9IGZyb20gJy4vYm9yZGVybGVzcy1idG4uZGlyZWN0aXZlJztcbmltcG9ydCB7IEFjdGl2aXR5U2VydmljZSB9IGZyb20gXCIuL2FwcC5zZXJ2aWNlXCI7XG5cbkBOZ01vZHVsZSh7XG4gIHByb3ZpZGVyczogW1xuICAgIEFjdGl2aXR5U2VydmljZVxuXSxcbiAgaW1wb3J0czogW1xuICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRVSVNpZGVEcmF3ZXJNb2R1bGUsXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIFNpZGVEcmF3ZXJQYWdlQ29tcG9uZW50LFxuICAgIEJvcmRlcmxlc3NCdG5EaXJlY3RpdmVcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIFNpZGVEcmF3ZXJQYWdlQ29tcG9uZW50LFxuICAgIEJvcmRlcmxlc3NCdG5EaXJlY3RpdmVcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBTaGFyZWRNb2R1bGUge1xuXG59XG4iXX0=