"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var home_component_1 = require("./home/home.component");
var about_component_1 = require("./about/about.component");
var contact_component_1 = require("./contact/contact.component");
var aboutlocker_component_1 = require("./aboutlocker/aboutlocker.component");
var feedback_component_1 = require("./feedback/feedback.component");
var accountsettings_component_1 = require("./accountsettings/accountsettings.component");
var newconnection_component_1 = require("./newconnection/newconnection.component");
var ratethisapp_component_1 = require("./ratethisapp/ratethisapp.component");
var users_component_1 = require("./users/users.component");
var http_1 = require("nativescript-angular/http");
var shared_1 = require("./shared");
var app_service_1 = require("../app.service");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            providers: [
                app_service_1.ActivityService
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_module_1.AppRoutingModule,
                shared_1.SharedModule,
                http_1.NativeScriptHttpModule
            ],
            declarations: [
                app_component_1.AppComponent,
                home_component_1.HomeComponent,
                about_component_1.AboutComponent,
                contact_component_1.ContactComponent,
                aboutlocker_component_1.AboutlockerComponent,
                feedback_component_1.FeedbackComponent,
                accountsettings_component_1.AccountsettingsComponent,
                newconnection_component_1.NewconnectionComponent,
                ratethisapp_component_1.RatethisappComponent,
                users_component_1.UsersComponent
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFFM0QsZ0ZBQThFO0FBRTlFLDJEQUF3RDtBQUN4RCxpREFBK0M7QUFDL0Msd0RBQXNEO0FBQ3RELDJEQUF5RDtBQUN6RCxpRUFBK0Q7QUFDL0QsNkVBQTJFO0FBQzNFLG9FQUFrRTtBQUNsRSx5RkFBdUY7QUFDdkYsbUZBQWlGO0FBQ2pGLDZFQUEyRTtBQUMzRSwyREFBeUQ7QUFDekQsa0RBQW1FO0FBQ25FLG1DQUF3QztBQUN4Qyw4Q0FBaUQ7QUEyQmpEO0lBQUE7SUFFQSxDQUFDO0lBRlksU0FBUztRQXhCckIsZUFBUSxDQUFDO1lBQ1IsU0FBUyxFQUFFO2dCQUNULDZCQUFlO2FBQ2xCO1lBQ0MsT0FBTyxFQUFFO2dCQUNQLHdDQUFrQjtnQkFDbEIscUNBQWdCO2dCQUNoQixxQkFBWTtnQkFDWiw2QkFBc0I7YUFDdkI7WUFDRCxZQUFZLEVBQUU7Z0JBQ1osNEJBQVk7Z0JBQ1osOEJBQWE7Z0JBQ2IsZ0NBQWM7Z0JBQ2Qsb0NBQWdCO2dCQUNoQiw0Q0FBb0I7Z0JBQ3BCLHNDQUFpQjtnQkFDakIsb0RBQXdCO2dCQUN4QixnREFBc0I7Z0JBQ3RCLDRDQUFvQjtnQkFDcEIsZ0NBQWM7YUFDZjtZQUNELFNBQVMsRUFBRSxDQUFDLDRCQUFZLENBQUM7U0FDMUIsQ0FBQztPQUNXLFNBQVMsQ0FFckI7SUFBRCxnQkFBQztDQUFBLEFBRkQsSUFFQztBQUZZLDhCQUFTIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL25hdGl2ZXNjcmlwdC5tb2R1bGUnO1xyXG5cclxuaW1wb3J0IHsgQXBwUm91dGluZ01vZHVsZSB9IGZyb20gJy4vYXBwLXJvdXRpbmcubW9kdWxlJztcclxuaW1wb3J0IHsgQXBwQ29tcG9uZW50IH0gZnJvbSAnLi9hcHAuY29tcG9uZW50JztcclxuaW1wb3J0IHsgSG9tZUNvbXBvbmVudCB9IGZyb20gJy4vaG9tZS9ob21lLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFib3V0Q29tcG9uZW50IH0gZnJvbSAnLi9hYm91dC9hYm91dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb250YWN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb250YWN0L2NvbnRhY3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQWJvdXRsb2NrZXJDb21wb25lbnQgfSBmcm9tICcuL2Fib3V0bG9ja2VyL2Fib3V0bG9ja2VyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZlZWRiYWNrQ29tcG9uZW50IH0gZnJvbSAnLi9mZWVkYmFjay9mZWVkYmFjay5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBY2NvdW50c2V0dGluZ3NDb21wb25lbnQgfSBmcm9tICcuL2FjY291bnRzZXR0aW5ncy9hY2NvdW50c2V0dGluZ3MuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmV3Y29ubmVjdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vbmV3Y29ubmVjdGlvbi9uZXdjb25uZWN0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFJhdGV0aGlzYXBwQ29tcG9uZW50IH0gZnJvbSAnLi9yYXRldGhpc2FwcC9yYXRldGhpc2FwcC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBVc2Vyc0NvbXBvbmVudCB9IGZyb20gJy4vdXNlcnMvdXNlcnMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwXCI7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4vc2hhcmVkJztcclxuaW1wb3J0IHsgQWN0aXZpdHlTZXJ2aWNlIH0gZnJvbSBcIi4uL2FwcC5zZXJ2aWNlXCI7XHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBwcm92aWRlcnM6IFtcclxuICAgIEFjdGl2aXR5U2VydmljZVxyXG5dLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcclxuICAgIEFwcFJvdXRpbmdNb2R1bGUsXHJcbiAgICBTaGFyZWRNb2R1bGUsXHJcbiAgICBOYXRpdmVTY3JpcHRIdHRwTW9kdWxlXHJcbiAgXSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIEFwcENvbXBvbmVudCxcclxuICAgIEhvbWVDb21wb25lbnQsXHJcbiAgICBBYm91dENvbXBvbmVudCxcclxuICAgIENvbnRhY3RDb21wb25lbnQsXHJcbiAgICBBYm91dGxvY2tlckNvbXBvbmVudCxcclxuICAgIEZlZWRiYWNrQ29tcG9uZW50LFxyXG4gICAgQWNjb3VudHNldHRpbmdzQ29tcG9uZW50LFxyXG4gICAgTmV3Y29ubmVjdGlvbkNvbXBvbmVudCxcclxuICAgIFJhdGV0aGlzYXBwQ29tcG9uZW50LFxyXG4gICAgVXNlcnNDb21wb25lbnRcclxuICBdLFxyXG4gIGJvb3RzdHJhcDogW0FwcENvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7XHJcblxyXG59XHJcbiJdfQ==