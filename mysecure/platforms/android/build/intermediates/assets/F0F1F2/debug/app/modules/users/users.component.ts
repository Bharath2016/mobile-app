import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'users',
  templateUrl: 'modules/users/users.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersComponent {
  text: string = 'Users';
}
