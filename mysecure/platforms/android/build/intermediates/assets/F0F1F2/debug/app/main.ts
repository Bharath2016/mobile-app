// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "nativescript-angular/platform";

import { AppModule } from "./modules/app.module";

import { Observable } from "tns-core-modules/data/observable";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { ios as iosUtils } from "tns-core-modules/utils/utils";
import { isIOS } from "tns-core-modules/platform";
import { AddEventListenerResult } from "nativescript-plugin-firebase";
import * as fs from "tns-core-modules/file-system";

const dialogs = require("ui/dialogs");
const firebase = require("nativescript-plugin-firebase");


let deviceToken ="";

setTimeout(function() {
 let firebase  = require("nativescript-plugin-firebase");
 
 let clipboard = require("nativescript-clipboard");
 firebase.init({
   onPushTokenReceivedCallback: function(token) {
      dialogs.alert("--onPushTokenReceivedCallback token :" + token);
      clipboard.setText(token).then(function() 
      { console.log("OK, copied to the clipboard"); });
      deviceToken=token;
      console.log("Firebase push token: " + token);
   },
   onMessageReceivedCallback: function(message) {
    dialogs.alert({
       title: "Push message: " + 
       (message.title !== undefined ? message.title : ""),
       message: JSON.stringify(message),
       okButtonText: "OK"
     }); 
      dialogs.alert("--onMessageReceivedCallback deviceToken :" + deviceToken);
      clipboard.setText(deviceToken).then(function() 
      { console.log("OK, copied to the clipboard"); });
     // clipboard.setText(deviceToken).then(function() 
     { console.log("OK, copied to the clipboard"); }      
   },
   //persist should be set to false as otherwise numbers aren't returned during livesync
   persist: false,
   //storageBucket: 'gs://yowwlr.appspot.com',
   onAuthStateChanged: (data: any) => {
     console.log(JSON.stringify(data))
     if (data.loggedIn) {
       //nn// BackendService.token = data.user.uid;
     }
     else {
      //nn// BackendService.token = "";
     }
   }
 }).then(
     function (instance) {
       console.log("firebase.init done");
     },
     function (error) {
       console.log("firebase.init error: " + error);
     }
 );
}, 3000);

firebase.login({
  type: firebase.LoginType.GOOGLE,
  // Optional 

  
  googleOptions: {
    hostedDomain: "bhrthmahesh09@gmail.com"
  }
}).then(
    function (result) {
      JSON.stringify(result);
    },
    function (errorMessage) {
      console.log(errorMessage);
    }
);

platformNativeScriptDynamic().bootstrapModule(AppModule);