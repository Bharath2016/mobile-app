"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var home_component_1 = require("./home/home.component");
var about_component_1 = require("./about/about.component");
var contact_component_1 = require("./contact/contact.component");
var aboutlocker_component_1 = require("./aboutlocker/aboutlocker.component");
var feedback_component_1 = require("./feedback/feedback.component");
var accountsettings_component_1 = require("./accountsettings/accountsettings.component");
var newconnection_component_1 = require("./newconnection/newconnection.component");
var ratethisapp_component_1 = require("./ratethisapp/ratethisapp.component");
var users_component_1 = require("./users/users.component");
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.NativeScriptRouterModule.forRoot([
                    { path: '', component: home_component_1.HomeComponent },
                    { path: 'about', component: about_component_1.AboutComponent },
                    { path: 'contact', component: contact_component_1.ContactComponent },
                    { path: 'aboutlocker', component: aboutlocker_component_1.AboutlockerComponent },
                    { path: 'feedback', component: feedback_component_1.FeedbackComponent },
                    { path: 'accountsettings', component: accountsettings_component_1.AccountsettingsComponent },
                    { path: 'newconnection', component: newconnection_component_1.NewconnectionComponent },
                    { path: 'ratethisapp', component: ratethisapp_component_1.RatethisappComponent },
                    { path: 'users', component: users_component_1.UsersComponent }
                ])
            ],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlDO0FBRXpDLHNEQUF1RTtBQUV2RSx3REFBc0Q7QUFDdEQsMkRBQXlEO0FBQ3pELGlFQUErRDtBQUMvRCw2RUFBMkU7QUFDM0Usb0VBQWtFO0FBQ2xFLHlGQUF1RjtBQUN2RixtRkFBaUY7QUFDakYsNkVBQTJFO0FBQzNFLDJEQUF5RDtBQWtCekQ7SUFBQTtJQUVBLENBQUM7SUFGWSxnQkFBZ0I7UUFoQjVCLGVBQVEsQ0FBQztZQUNSLE9BQU8sRUFBRTtnQkFDUCxpQ0FBd0IsQ0FBQyxPQUFPLENBQUM7b0JBQy9CLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsOEJBQWEsRUFBRTtvQkFDdEMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxnQ0FBYyxFQUFFO29CQUM1QyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLG9DQUFnQixFQUFFO29CQUNoRCxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLDRDQUFvQixFQUFFO29CQUN4RCxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLHNDQUFpQixFQUFFO29CQUNsRCxFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsb0RBQXdCLEVBQUU7b0JBQ2hFLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxTQUFTLEVBQUUsZ0RBQXNCLEVBQUU7b0JBQzVELEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxTQUFTLEVBQUUsNENBQW9CLEVBQUU7b0JBQ3hELEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsZ0NBQWMsRUFBRTtpQkFDN0MsQ0FBQzthQUNIO1lBQ0QsT0FBTyxFQUFFLENBQUMsaUNBQXdCLENBQUM7U0FDcEMsQ0FBQztPQUNXLGdCQUFnQixDQUU1QjtJQUFELHVCQUFDO0NBQUEsQUFGRCxJQUVDO0FBRlksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcblxuaW1wb3J0IHsgSG9tZUNvbXBvbmVudCB9IGZyb20gJy4vaG9tZS9ob21lLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBYm91dENvbXBvbmVudCB9IGZyb20gJy4vYWJvdXQvYWJvdXQuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRhY3RDb21wb25lbnQgfSBmcm9tICcuL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQWJvdXRsb2NrZXJDb21wb25lbnQgfSBmcm9tICcuL2Fib3V0bG9ja2VyL2Fib3V0bG9ja2VyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGZWVkYmFja0NvbXBvbmVudCB9IGZyb20gJy4vZmVlZGJhY2svZmVlZGJhY2suY29tcG9uZW50JztcbmltcG9ydCB7IEFjY291bnRzZXR0aW5nc0NvbXBvbmVudCB9IGZyb20gJy4vYWNjb3VudHNldHRpbmdzL2FjY291bnRzZXR0aW5ncy5jb21wb25lbnQnO1xuaW1wb3J0IHsgTmV3Y29ubmVjdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vbmV3Y29ubmVjdGlvbi9uZXdjb25uZWN0aW9uLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBSYXRldGhpc2FwcENvbXBvbmVudCB9IGZyb20gJy4vcmF0ZXRoaXNhcHAvcmF0ZXRoaXNhcHAuY29tcG9uZW50JztcbmltcG9ydCB7IFVzZXJzQ29tcG9uZW50IH0gZnJvbSAnLi91c2Vycy91c2Vycy5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvclJvb3QoW1xuICAgICAgeyBwYXRoOiAnJywgY29tcG9uZW50OiBIb21lQ29tcG9uZW50IH0sXG4gICAgICB7IHBhdGg6ICdhYm91dCcsIGNvbXBvbmVudDogQWJvdXRDb21wb25lbnQgfSxcbiAgICAgIHsgcGF0aDogJ2NvbnRhY3QnLCBjb21wb25lbnQ6IENvbnRhY3RDb21wb25lbnQgfSxcbiAgICAgIHsgcGF0aDogJ2Fib3V0bG9ja2VyJywgY29tcG9uZW50OiBBYm91dGxvY2tlckNvbXBvbmVudCB9LFxuICAgICAgeyBwYXRoOiAnZmVlZGJhY2snLCBjb21wb25lbnQ6IEZlZWRiYWNrQ29tcG9uZW50IH0sXG4gICAgICB7IHBhdGg6ICdhY2NvdW50c2V0dGluZ3MnLCBjb21wb25lbnQ6IEFjY291bnRzZXR0aW5nc0NvbXBvbmVudCB9LFxuICAgICAgeyBwYXRoOiAnbmV3Y29ubmVjdGlvbicsIGNvbXBvbmVudDogTmV3Y29ubmVjdGlvbkNvbXBvbmVudCB9LFxuICAgICAgeyBwYXRoOiAncmF0ZXRoaXNhcHAnLCBjb21wb25lbnQ6IFJhdGV0aGlzYXBwQ29tcG9uZW50IH0sXG4gICAgICB7IHBhdGg6ICd1c2VycycsIGNvbXBvbmVudDogVXNlcnNDb21wb25lbnQgfVxuICAgIF0pXG4gIF0sXG4gIGV4cG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVdXG59KVxuZXhwb3J0IGNsYXNzIEFwcFJvdXRpbmdNb2R1bGUge1xuXG59XG4iXX0=