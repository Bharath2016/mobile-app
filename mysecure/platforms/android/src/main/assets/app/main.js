"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// this import should be first in order to load some required settings (like globals and reflect-metadata)
var platform_1 = require("nativescript-angular/platform");
var app_module_1 = require("./modules/app.module");
var dialogs = require("ui/dialogs");
var firebase = require("nativescript-plugin-firebase");
var deviceToken = "";
setTimeout(function () {
    var firebase = require("nativescript-plugin-firebase");
    var clipboard = require("nativescript-clipboard");
    firebase.init({
        onPushTokenReceivedCallback: function (token) {
            dialogs.alert("--onPushTokenReceivedCallback token :" + token);
            clipboard.setText(token).then(function () { console.log("OK, copied to the clipboard"); });
            deviceToken = token;
            console.log("Firebase push token: " + token);
        },
        onMessageReceivedCallback: function (message) {
            dialogs.alert({
                title: "Push message: " +
                    (message.title !== undefined ? message.title : ""),
                message: JSON.stringify(message),
                okButtonText: "OK"
            });
            dialogs.alert("--onMessageReceivedCallback deviceToken :" + deviceToken);
            clipboard.setText(deviceToken).then(function () { console.log("OK, copied to the clipboard"); });
            // clipboard.setText(deviceToken).then(function() 
            {
                console.log("OK, copied to the clipboard");
            }
        },
        //persist should be set to false as otherwise numbers aren't returned during livesync
        persist: false,
        //storageBucket: 'gs://yowwlr.appspot.com',
        onAuthStateChanged: function (data) {
            console.log(JSON.stringify(data));
            if (data.loggedIn) {
                //nn// BackendService.token = data.user.uid;
            }
            else {
                //nn// BackendService.token = "";
            }
        }
    }).then(function (instance) {
        console.log("firebase.init done");
    }, function (error) {
        console.log("firebase.init error: " + error);
    });
}, 3000);
firebase.login({
    type: firebase.LoginType.GOOGLE,
    // Optional 
    googleOptions: {
        hostedDomain: "bhrthmahesh09@gmail.com"
    }
}).then(function (result) {
    JSON.stringify(result);
}, function (errorMessage) {
    console.log(errorMessage);
});
platform_1.platformNativeScriptDynamic().bootstrapModule(app_module_1.AppModule);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwwR0FBMEc7QUFDMUcsMERBQTRFO0FBRTVFLG1EQUFpRDtBQVNqRCxJQUFNLE9BQU8sR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDdEMsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLDhCQUE4QixDQUFDLENBQUM7QUFHekQsSUFBSSxXQUFXLEdBQUUsRUFBRSxDQUFDO0FBRXBCLFVBQVUsQ0FBQztJQUNWLElBQUksUUFBUSxHQUFJLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0lBRXhELElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0lBQ2xELFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDWiwyQkFBMkIsRUFBRSxVQUFTLEtBQUs7WUFDeEMsT0FBTyxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUMvRCxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRCxXQUFXLEdBQUMsS0FBSyxDQUFDO1lBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDaEQsQ0FBQztRQUNELHlCQUF5QixFQUFFLFVBQVMsT0FBTztZQUMxQyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUNYLEtBQUssRUFBRSxnQkFBZ0I7b0JBQ3ZCLENBQUMsT0FBTyxDQUFDLEtBQUssS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDbEQsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDO2dCQUNoQyxZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDLENBQUM7WUFDRixPQUFPLENBQUMsS0FBSyxDQUFDLDJDQUEyQyxHQUFHLFdBQVcsQ0FBQyxDQUFDO1lBQ3pFLFNBQVMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xELGtEQUFrRDtZQUNsRCxDQUFDO2dCQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLENBQUMsQ0FBQztZQUFDLENBQUM7UUFDakQsQ0FBQztRQUNELHFGQUFxRjtRQUNyRixPQUFPLEVBQUUsS0FBSztRQUNkLDJDQUEyQztRQUMzQyxrQkFBa0IsRUFBRSxVQUFDLElBQVM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7WUFDakMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLDRDQUE0QztZQUM5QyxDQUFDO1lBQ0QsSUFBSSxDQUFDLENBQUM7Z0JBQ0wsaUNBQWlDO1lBQ2xDLENBQUM7UUFDSCxDQUFDO0tBQ0YsQ0FBQyxDQUFDLElBQUksQ0FDSCxVQUFVLFFBQVE7UUFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQ3BDLENBQUMsRUFDRCxVQUFVLEtBQUs7UUFDYixPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixHQUFHLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUMsQ0FDSixDQUFDO0FBQ0gsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0FBRVQsUUFBUSxDQUFDLEtBQUssQ0FBQztJQUNiLElBQUksRUFBRSxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU07SUFDL0IsWUFBWTtJQUdaLGFBQWEsRUFBRTtRQUNiLFlBQVksRUFBRSx5QkFBeUI7S0FDeEM7Q0FDRixDQUFDLENBQUMsSUFBSSxDQUNILFVBQVUsTUFBTTtJQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDekIsQ0FBQyxFQUNELFVBQVUsWUFBWTtJQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQzVCLENBQUMsQ0FDSixDQUFDO0FBRUYsc0NBQTJCLEVBQUUsQ0FBQyxlQUFlLENBQUMsc0JBQVMsQ0FBQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLy8gdGhpcyBpbXBvcnQgc2hvdWxkIGJlIGZpcnN0IGluIG9yZGVyIHRvIGxvYWQgc29tZSByZXF1aXJlZCBzZXR0aW5ncyAobGlrZSBnbG9iYWxzIGFuZCByZWZsZWN0LW1ldGFkYXRhKVxuaW1wb3J0IHsgcGxhdGZvcm1OYXRpdmVTY3JpcHREeW5hbWljIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3BsYXRmb3JtXCI7XG5cbmltcG9ydCB7IEFwcE1vZHVsZSB9IGZyb20gXCIuL21vZHVsZXMvYXBwLm1vZHVsZVwiO1xuXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlXCI7XG5pbXBvcnQgeyBhbGVydCwgcHJvbXB0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0IHsgaW9zIGFzIGlvc1V0aWxzIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdXRpbHMvdXRpbHNcIjtcbmltcG9ydCB7IGlzSU9TIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvcGxhdGZvcm1cIjtcbmltcG9ydCB7IEFkZEV2ZW50TGlzdGVuZXJSZXN1bHQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXBsdWdpbi1maXJlYmFzZVwiO1xuaW1wb3J0ICogYXMgZnMgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZmlsZS1zeXN0ZW1cIjtcblxuY29uc3QgZGlhbG9ncyA9IHJlcXVpcmUoXCJ1aS9kaWFsb2dzXCIpO1xuY29uc3QgZmlyZWJhc2UgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXBsdWdpbi1maXJlYmFzZVwiKTtcblxuXG5sZXQgZGV2aWNlVG9rZW4gPVwiXCI7XG5cbnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gbGV0IGZpcmViYXNlICA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtcGx1Z2luLWZpcmViYXNlXCIpO1xuIFxuIGxldCBjbGlwYm9hcmQgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWNsaXBib2FyZFwiKTtcbiBmaXJlYmFzZS5pbml0KHtcbiAgIG9uUHVzaFRva2VuUmVjZWl2ZWRDYWxsYmFjazogZnVuY3Rpb24odG9rZW4pIHtcbiAgICAgIGRpYWxvZ3MuYWxlcnQoXCItLW9uUHVzaFRva2VuUmVjZWl2ZWRDYWxsYmFjayB0b2tlbiA6XCIgKyB0b2tlbik7XG4gICAgICBjbGlwYm9hcmQuc2V0VGV4dCh0b2tlbikudGhlbihmdW5jdGlvbigpIFxuICAgICAgeyBjb25zb2xlLmxvZyhcIk9LLCBjb3BpZWQgdG8gdGhlIGNsaXBib2FyZFwiKTsgfSk7XG4gICAgICBkZXZpY2VUb2tlbj10b2tlbjtcbiAgICAgIGNvbnNvbGUubG9nKFwiRmlyZWJhc2UgcHVzaCB0b2tlbjogXCIgKyB0b2tlbik7XG4gICB9LFxuICAgb25NZXNzYWdlUmVjZWl2ZWRDYWxsYmFjazogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgIGRpYWxvZ3MuYWxlcnQoe1xuICAgICAgIHRpdGxlOiBcIlB1c2ggbWVzc2FnZTogXCIgKyBcbiAgICAgICAobWVzc2FnZS50aXRsZSAhPT0gdW5kZWZpbmVkID8gbWVzc2FnZS50aXRsZSA6IFwiXCIpLFxuICAgICAgIG1lc3NhZ2U6IEpTT04uc3RyaW5naWZ5KG1lc3NhZ2UpLFxuICAgICAgIG9rQnV0dG9uVGV4dDogXCJPS1wiXG4gICAgIH0pOyBcbiAgICAgIGRpYWxvZ3MuYWxlcnQoXCItLW9uTWVzc2FnZVJlY2VpdmVkQ2FsbGJhY2sgZGV2aWNlVG9rZW4gOlwiICsgZGV2aWNlVG9rZW4pO1xuICAgICAgY2xpcGJvYXJkLnNldFRleHQoZGV2aWNlVG9rZW4pLnRoZW4oZnVuY3Rpb24oKSBcbiAgICAgIHsgY29uc29sZS5sb2coXCJPSywgY29waWVkIHRvIHRoZSBjbGlwYm9hcmRcIik7IH0pO1xuICAgICAvLyBjbGlwYm9hcmQuc2V0VGV4dChkZXZpY2VUb2tlbikudGhlbihmdW5jdGlvbigpIFxuICAgICB7IGNvbnNvbGUubG9nKFwiT0ssIGNvcGllZCB0byB0aGUgY2xpcGJvYXJkXCIpOyB9ICAgICAgXG4gICB9LFxuICAgLy9wZXJzaXN0IHNob3VsZCBiZSBzZXQgdG8gZmFsc2UgYXMgb3RoZXJ3aXNlIG51bWJlcnMgYXJlbid0IHJldHVybmVkIGR1cmluZyBsaXZlc3luY1xuICAgcGVyc2lzdDogZmFsc2UsXG4gICAvL3N0b3JhZ2VCdWNrZXQ6ICdnczovL3lvd3dsci5hcHBzcG90LmNvbScsXG4gICBvbkF1dGhTdGF0ZUNoYW5nZWQ6IChkYXRhOiBhbnkpID0+IHtcbiAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkoZGF0YSkpXG4gICAgIGlmIChkYXRhLmxvZ2dlZEluKSB7XG4gICAgICAgLy9ubi8vIEJhY2tlbmRTZXJ2aWNlLnRva2VuID0gZGF0YS51c2VyLnVpZDtcbiAgICAgfVxuICAgICBlbHNlIHtcbiAgICAgIC8vbm4vLyBCYWNrZW5kU2VydmljZS50b2tlbiA9IFwiXCI7XG4gICAgIH1cbiAgIH1cbiB9KS50aGVuKFxuICAgICBmdW5jdGlvbiAoaW5zdGFuY2UpIHtcbiAgICAgICBjb25zb2xlLmxvZyhcImZpcmViYXNlLmluaXQgZG9uZVwiKTtcbiAgICAgfSxcbiAgICAgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgY29uc29sZS5sb2coXCJmaXJlYmFzZS5pbml0IGVycm9yOiBcIiArIGVycm9yKTtcbiAgICAgfVxuICk7XG59LCAzMDAwKTtcblxuZmlyZWJhc2UubG9naW4oe1xuICB0eXBlOiBmaXJlYmFzZS5Mb2dpblR5cGUuR09PR0xFLFxuICAvLyBPcHRpb25hbCBcblxuICBcbiAgZ29vZ2xlT3B0aW9uczoge1xuICAgIGhvc3RlZERvbWFpbjogXCJiaHJ0aG1haGVzaDA5QGdtYWlsLmNvbVwiXG4gIH1cbn0pLnRoZW4oXG4gICAgZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgSlNPTi5zdHJpbmdpZnkocmVzdWx0KTtcbiAgICB9LFxuICAgIGZ1bmN0aW9uIChlcnJvck1lc3NhZ2UpIHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yTWVzc2FnZSk7XG4gICAgfVxuKTtcblxucGxhdGZvcm1OYXRpdmVTY3JpcHREeW5hbWljKCkuYm9vdHN0cmFwTW9kdWxlKEFwcE1vZHVsZSk7Il19