"use strict";
var Application = require("application");
var Dialogs = require("ui/dialogs");
var Utility = require("utils/utils");
var ApplicationSettings = require("application-settings");
var Ratings = (function () {
    function Ratings(configuration) {
        this.configuration = configuration;
        this.configuration.id = this.configuration.id ? this.configuration.id : "ratings-0";
        this.configuration.showOnCount = this.configuration.showOnCount ? this.configuration.showOnCount : 5;
        this.configuration.agreeButtonText = this.configuration.agreeButtonText ? this.configuration.agreeButtonText : "Rate Now";
        this.configuration.remindButtonText = this.configuration.remindButtonText ? this.configuration.remindButtonText : "Remind Me Later";
        this.configuration.declineButtonText = this.configuration.declineButtonText ? this.configuration.declineButtonText : "No Thanks";
    }
    Ratings.prototype.init = function () {
        this.showCount = ApplicationSettings.getNumber(this.configuration.id, 0);
        this.showCount++;
        ApplicationSettings.setNumber(this.configuration.id, this.showCount);
    };
    Ratings.prototype.prompt = function () {
        var _this = this;
        if (this.showCount == this.configuration.showOnCount) {
            Dialogs.confirm({
                title: this.configuration.title,
                message: this.configuration.text,
                okButtonText: this.configuration.agreeButtonText,
                cancelButtonText: this.configuration.declineButtonText,
                neutralButtonText: this.configuration.remindButtonText
            }).then(function (result) {
                if (result == true) {
                    var appStore = "";
                    if (Application.android) {
                        var androidPackageName = _this.configuration.androidPackageId ? _this.configuration.androidPackageId : Application.android.packageName;
                        var uri = android.net.Uri.parse("market://details?id=" + androidPackageName);
                        var myAppLinkToMarket = new android.content.Intent(android.content.Intent.ACTION_VIEW, uri);
                        // Launch the PlayStore
                        Application.android.foregroundActivity.startActivity(myAppLinkToMarket);
                    }
                    else if (Application.ios) {
                        appStore = "itms-apps://itunes.apple.com/en/app/id" + _this.configuration.iTunesAppId;
                    }
                    Utility.openUrl(appStore);
                }
                else if (result == false) {
                }
                else {
                    ApplicationSettings.setNumber(_this.configuration.id, 0);
                }
            });
        }
    };
    return Ratings;
}());
exports.Ratings = Ratings;
