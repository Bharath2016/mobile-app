"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var image_1 = require("ui/image");
var label_1 = require("ui/label");
var URL = "https://docs.nativescript.org/img/cli-getting-started/angular/chapter0/NativeScript_logo.png";
var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.onStackLoaded = function (args) {
        var stackView = args.object;
        this.newImage = new image_1.Image();
        this.newImage.src = URL;
        this.newImage.stretch = "none";
        this.newImage.style.margin = "15";
        this.newLabel = new label_1.Label();
        this.newLabel.text = "Image loaded from code behind";
        this.newLabel.style.margin = "15";
        stackView.addChild(this.newLabel);
        stackView.addChild(this.newImage);
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'home',
            templateUrl: 'modules/home/home.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        })
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFtRTtBQUNuRSxrQ0FBaUM7QUFFakMsa0NBQWlDO0FBRWpDLElBQU0sR0FBRyxHQUFHLDhGQUE4RixDQUFDO0FBUzNHO0lBQUE7SUFtQkEsQ0FBQztJQWZDLHFDQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2QsSUFBSSxTQUFTLEdBQWdCLElBQUksQ0FBQyxNQUFNLENBQUM7UUFFekMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLGFBQUssRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUN4QixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUVsQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksYUFBSyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsK0JBQStCLENBQUM7UUFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUVsQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNsQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBbEJVLGFBQWE7UUFMekIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNO1lBQ2hCLFdBQVcsRUFBRSxrQ0FBa0M7WUFDL0MsZUFBZSxFQUFFLDhCQUF1QixDQUFDLE1BQU07U0FDaEQsQ0FBQztPQUNXLGFBQWEsQ0FtQnpCO0lBQUQsb0JBQUM7Q0FBQSxBQW5CRCxJQW1CQztBQW5CWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEltYWdlIH0gZnJvbSBcInVpL2ltYWdlXCI7XG5pbXBvcnQgeyBTdGFja0xheW91dCB9IGZyb20gXCJ1aS9sYXlvdXRzL3N0YWNrLWxheW91dFwiO1xuaW1wb3J0IHsgTGFiZWwgfSBmcm9tIFwidWkvbGFiZWxcIjtcblxuY29uc3QgVVJMID0gXCJodHRwczovL2RvY3MubmF0aXZlc2NyaXB0Lm9yZy9pbWcvY2xpLWdldHRpbmctc3RhcnRlZC9hbmd1bGFyL2NoYXB0ZXIwL05hdGl2ZVNjcmlwdF9sb2dvLnBuZ1wiO1xuXG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaG9tZScsXG4gIHRlbXBsYXRlVXJsOiAnbW9kdWxlcy9ob21lL2hvbWUuY29tcG9uZW50Lmh0bWwnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IHtcbiAgcHVibGljIG5ld0ltYWdlOiBJbWFnZTtcbiAgcHVibGljIG5ld0xhYmVsOiBMYWJlbDtcblxuICBvblN0YWNrTG9hZGVkKGFyZ3MpIHtcbiAgICAgIGxldCBzdGFja1ZpZXcgPSA8U3RhY2tMYXlvdXQ+YXJncy5vYmplY3Q7XG5cbiAgICAgIHRoaXMubmV3SW1hZ2UgPSBuZXcgSW1hZ2UoKTtcbiAgICAgIHRoaXMubmV3SW1hZ2Uuc3JjID0gVVJMO1xuICAgICAgdGhpcy5uZXdJbWFnZS5zdHJldGNoID0gXCJub25lXCI7XG4gICAgICB0aGlzLm5ld0ltYWdlLnN0eWxlLm1hcmdpbiA9IFwiMTVcIjtcblxuICAgICAgdGhpcy5uZXdMYWJlbCA9IG5ldyBMYWJlbCgpO1xuICAgICAgdGhpcy5uZXdMYWJlbC50ZXh0ID0gXCJJbWFnZSBsb2FkZWQgZnJvbSBjb2RlIGJlaGluZFwiO1xuICAgICAgdGhpcy5uZXdMYWJlbC5zdHlsZS5tYXJnaW4gPSBcIjE1XCI7XG5cbiAgICAgIHN0YWNrVmlldy5hZGRDaGlsZCh0aGlzLm5ld0xhYmVsKTtcbiAgICAgIHN0YWNrVmlldy5hZGRDaGlsZCh0aGlzLm5ld0ltYWdlKTtcbiAgfVxufVxuIl19