"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_ratings_1 = require("nativescript-ratings");
var RatethisappComponent = /** @class */ (function () {
    function RatethisappComponent() {
        var ratings = new nativescript_ratings_1.Ratings({
            id: "0",
            showOnCount: 2,
            title: "What do you think?",
            text: "If you like this app, please take a moment to leave a positive rating.",
            agreeButtonText: "Rate Now",
            remindButtonText: "Remind Me Later",
            declineButtonText: "No Thanks",
            androidPackageId: "com.nativescript.demo",
            iTunesAppId: "927183647"
        });
        ratings.init();
        ratings.prompt();
    }
    RatethisappComponent = __decorate([
        core_1.Component({
            selector: 'ratethisapp',
            templateUrl: 'modules/ratethisapp/ratethisapp.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [])
    ], RatethisappComponent);
    return RatethisappComponent;
}());
exports.RatethisappComponent = RatethisappComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmF0ZXRoaXNhcHAuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmF0ZXRoaXNhcHAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQW1FO0FBQ25FLDZEQUErQztBQU0vQztJQUNFO1FBQ0UsSUFBSSxPQUFPLEdBQUcsSUFBSSw4QkFBTyxDQUN2QjtZQUNJLEVBQUUsRUFBRSxHQUFHO1lBQ1AsV0FBVyxFQUFFLENBQUM7WUFDZCxLQUFLLEVBQUUsb0JBQW9CO1lBQzNCLElBQUksRUFBRSx3RUFBd0U7WUFDOUUsZUFBZSxFQUFFLFVBQVU7WUFDM0IsZ0JBQWdCLEVBQUUsaUJBQWlCO1lBQ25DLGlCQUFpQixFQUFFLFdBQVc7WUFDOUIsZ0JBQWdCLEVBQUUsdUJBQXVCO1lBQ3pDLFdBQVcsRUFBRSxXQUFXO1NBQzNCLENBQ0osQ0FBQztRQUNBLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNmLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBakJZLG9CQUFvQjtRQUxoQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGFBQWE7WUFDdkIsV0FBVyxFQUFFLGdEQUFnRDtZQUM3RCxlQUFlLEVBQUUsOEJBQXVCLENBQUMsTUFBTTtTQUNoRCxDQUFDOztPQUNXLG9CQUFvQixDQWtCaEM7SUFBRCwyQkFBQztDQUFBLEFBbEJELElBa0JDO0FBbEJZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJhdGluZ3MgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXJhdGluZ3NcIjtcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3JhdGV0aGlzYXBwJyxcbiAgdGVtcGxhdGVVcmw6ICdtb2R1bGVzL3JhdGV0aGlzYXBwL3JhdGV0aGlzYXBwLmNvbXBvbmVudC5odG1sJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcbn0pXG5leHBvcnQgY2xhc3MgUmF0ZXRoaXNhcHBDb21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBsZXQgcmF0aW5ncyA9IG5ldyBSYXRpbmdzKFxuICAgICAge1xuICAgICAgICAgIGlkOiBcIjBcIixcbiAgICAgICAgICBzaG93T25Db3VudDogMixcbiAgICAgICAgICB0aXRsZTogXCJXaGF0IGRvIHlvdSB0aGluaz9cIixcbiAgICAgICAgICB0ZXh0OiBcIklmIHlvdSBsaWtlIHRoaXMgYXBwLCBwbGVhc2UgdGFrZSBhIG1vbWVudCB0byBsZWF2ZSBhIHBvc2l0aXZlIHJhdGluZy5cIixcbiAgICAgICAgICBhZ3JlZUJ1dHRvblRleHQ6IFwiUmF0ZSBOb3dcIixcbiAgICAgICAgICByZW1pbmRCdXR0b25UZXh0OiBcIlJlbWluZCBNZSBMYXRlclwiLFxuICAgICAgICAgIGRlY2xpbmVCdXR0b25UZXh0OiBcIk5vIFRoYW5rc1wiLFxuICAgICAgICAgIGFuZHJvaWRQYWNrYWdlSWQ6IFwiY29tLm5hdGl2ZXNjcmlwdC5kZW1vXCIsXG4gICAgICAgICAgaVR1bmVzQXBwSWQ6IFwiOTI3MTgzNjQ3XCJcbiAgICAgIH1cbiAgKTtcbiAgICByYXRpbmdzLmluaXQoKTtcbiAgICByYXRpbmdzLnByb21wdCgpO1xufVxufVxuIl19