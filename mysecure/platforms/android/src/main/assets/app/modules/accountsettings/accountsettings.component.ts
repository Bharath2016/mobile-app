import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'accountsettings',
  templateUrl: 'modules/accountsettings/accountsettings.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountsettingsComponent {
  text: string = 'Account Settings';
}
